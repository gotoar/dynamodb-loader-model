// const mocha = require('mocha')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

const data = [
  // Passes
  {
    key: {
      id: '1',
      second_id: '2'
    },
    payload: {
      id: '1',
      second_id: '2',
      name: 'hola'
    }
  },
  // Fails
  {
    key: {
      id: '42'
    },
    payload: {
      id: '42',
      name: 'hola'
    }
  },
  // Passes
  {
    key: {
      id: '276542',
      second_id: '2'
    },
    payload: {
      id: '276542',
      second_id: '2',
      name: 'hola soy el 2',
      arr: ['two', 'elements']
    }
  },
  // Passes
  {
    key: {
      id: '276542',
      second_id: '3'
    },
    payload: {
      id: '276542',
      second_id: '3',
      name: 'hola soy el 2 pero en otro customer'
    }
  },
  // Passes
  {
    key: {
      id: '123123',
      second_id: '2'
    },
    payload: {
      id: '123123',
      second_id: '2',
      name: 'hola soy el 3'
    }
  },
  // Passes
  {
    key: {
      id: '123123',
      second_id: '2'
    },
    payload: {
      id: '123123',
      second_id: '2',
      name: 'hola soy el 4',
      avatar: ''
    }
  }
]

const { Model, ModelManager } = require('../classes/model')

const rangeConfig = {
  hashKey: 'id',
  rangeKey: 'second_id',
  fields: {
    id: { type: 'uuid' },
    second_id: { type: 'string', required: true },
    name: { type: 'string' },
    avatar: { type: 'string' },
    tags: { type: 'string' },
    arr: { type: 'array', default: [] },
    request: { type: 'object' },
    number: { type: 'number' },
    boolean: { type: 'boolean' }
  }
}

if (process.env.NODE_ENV === 'testing') {
  rangeConfig.dynamo = {
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  }
}

class RangeModel extends Model {
  constructor () {
    super(
      'table_range',
      rangeConfig
    )
  }
}

const rangeModel = new RangeModel()

describe('Generates an item', () => {
  it('should generate the item successfully', async () => {
    const item = rangeModel.generateItem(data[0].payload)
    expect(item).to.not.equal(false)
  })

  it('should generate the item successfully but without the unrequired, undefaulted fields', async () => {
    const item = rangeModel.generateItem(data[0].payload)
    expect(item).to.not.equal(false)
    expect(item)
      .to.have.property('arr')
      .but.not.property('avatar')
  })

  it('should fail because it is missing a required field', async () => {
    const item = rangeModel.generateItem(data[1].payload)
    expect(item).to.equal(false)
  })
})

describe('Inserts an item', () => {
  before(async () => {
  })

  it('should insert the item', async () => {
    await rangeModel.create(data[0].payload)
    // expect(rangeModel._dataset()).to.have.length(1)
  })

  it('should not insert the item because it is missing a required field', async () => {
    try {
      await rangeModel.create(data[1].payload)
    } catch (err) {
      expect(err).to.have.property('message').and.contain('Missing a required field')
      expect(err.message).to.contain('second_id')
    }
  })
})

describe('Fetches an item', () => {
  before(async () => {
    await Promise.all([
      rangeModel.create(data[0].payload),
      rangeModel.create(data[2].payload),
      rangeModel.create(data[3].payload),
      rangeModel.remove(data[4].key)
    ])
  })

  it('should fetch the first item', async () => {
    const res = await rangeModel.fetch(data[0].key)
    expect(res).to.deep.equal(rangeModel.generateItem(data[0].payload))
  })

  it('should fetch the second item', async () => {
    const res = await rangeModel.fetch(data[2].key)
    expect(res).to.deep.equal(rangeModel.generateItem(data[2].payload))
  })

  it('should fetch the third item', async () => {
    const res = await rangeModel.fetch(data[3].key)
    expect(res).to.deep.equal(rangeModel.generateItem(data[3].payload))
  })

  it('should fetch an unloaded item and get undefined', async () => {
    const res = await rangeModel.fetch(data[4].key)
    expect(res).to.equal(undefined)
  })
})

describe('Updates an item', () => {
  beforeEach(async () => {
    await Promise.all([
      rangeModel.create(data[0].payload),
      rangeModel.create(data[2].payload),
      rangeModel.create(data[3].payload),
      rangeModel.remove({ id: '__1', second_id: '__1' })
    ])
  })

  it('should update the first item', async () => {
    const newPayload = data[0].payload
    newPayload.name = 'new hola'
    const res = await rangeModel.update(data[0].key, newPayload)
    expect(res).to.deep.equal(rangeModel.generateItem(newPayload))
  })

  it('should add an item', async () => {
    const previous = await rangeModel.fetch(data[2].key)
    const previousLength = previous.arr.length
    const newArr = [ ...previous.arr.slice(0), 'added' ]
    const updated = await rangeModel.update(data[2].key, { ...previous, arr: newArr })
    expect(updated.arr).to.have.length(previousLength + 1)
  })

  it('should override items', async () => {
    const previous = await rangeModel.fetch(data[2].key)
    const newObject = {
      ...previous,
      arr: ['unique']
    }
    const updated = await rangeModel.update(data[2].key, newObject)
    expect(updated.arr).to.deep.equal(newObject.arr)
  })

  it('should remove unset keys with a boolean passed as option', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', avatar: 'hola', arr: ['hey'] }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey' }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2, true)
    expect(updated).to.not.have.property('avatar')
    expect(updated).to.have.property('arr').and.have.length(0)
  })

  it('should remove unset keys with an object passed as option', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', avatar: 'hola', arr: ['hey'] }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey' }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2, { override: true })
    expect(updated).to.not.have.property('avatar')
    expect(updated).to.have.property('arr').and.have.length(0)
  })

  it('should not remove unset keys', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', avatar: 'hola', arr: ['hey'] }
    const obj2 = { id: '__1', second_id: '__1', name: 'hola' }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('avatar')
    expect(updated).to.have.property('name').and.equal('hola')
    expect(updated).to.have.property('arr').and.have.length(1)
  })

  it('should not update if there were no changes', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'] }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'] }
    const previous = await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(previous).to.deep.equal(updated)
  })

  it('should create a deeply nested attribute', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'] }
    const nestedData = { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: nestedData }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('request').and.deep.equal(nestedData)
  })

  it('should update a deeply nested attribute', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const newNestedData = { prop1: { hey: 'hi2', hey2: 'hola2', hey3: { hi: 'hey' } }, heyroot: 'hey' }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: newNestedData }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('request').and.deep.equal(newNestedData)
  })

  it('should update a deeply nested attribute shallowly', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const newNestedData = { prop1: { hey: 'hi2', hey2: 'hola2', hey3: { hi: 'hey' } }, heyroot: 'hey' }
    const obj2 = { id: '__1', second_id: '__1', request: newNestedData }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2, { shallow: true })
    expect(updated).to.have.property('request').and.deep.equal(newNestedData)
    expect(updated).to.have.property('arr').and.deep.equal(obj1.arr)
  })

  it('should set a number to 0', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' }, number: 0 }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('number').and.equal(0)
  })

  it('should set a number to 1', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' }, number: 1 }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('number').and.equal(1)
  })

  it('should set a boolean to false', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' }, boolean: false }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('boolean').and.equal(false)
  })

  it('should set a boolean to true', async () => {
    const obj1 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' } }
    const obj2 = { id: '__1', second_id: '__1', name: 'hey', arr: ['item'], request: { prop1: { hey: 'hi', hey2: 'hola' }, heyroot: 'hey' }, boolean: true }
    await rangeModel.create(obj1)
    const updated = await rangeModel.update({ id: obj1.id, second_id: obj1.second_id }, obj2)
    expect(updated).to.have.property('boolean').and.equal(true)
  })
})

describe('Removes an item', () => {
  before(async () => {
    await Promise.all([
      rangeModel.create(data[0].payload)
    ])
  })
})

describe('Remove empty fields', () => {
  beforeEach(async () => {
  })

  it('should remove empty string fields in an object', async () => {
    const obj = { key1: 'hey', key2: '' }
    const res = rangeModel._removeEmptyStringElements(obj)
    expect(res).to.have.property('key1').and.not.have.key('key2')
  })

  it('should remove empty string fields before inserting', async () => {
    await rangeModel.create(data[5].payload)
    // expect(rangeModel._dataset()).to.have.length(1)
    const inserted = await rangeModel.fetch(data[5].key)
    expect(inserted).to.have.property('name').and.not.property('avatar')
  })
})

describe('ModelManager', () => {
  before(async () => {
    await rangeModel.remove(data[0].key)
  })

  it('should create an instance', async () => {
    const manager = new ModelManager(RangeModel)
    const modelInstance = manager.getInstance('1')
    expect(modelInstance).to.be.an.instanceOf(RangeModel)
  })

  it('different instances should have different cached values', async () => {
    const manager = new ModelManager(RangeModel)
    const modelInstance1 = manager.getInstance('1')
    const modelInstance2 = manager.getInstance('2')
    const before = await Promise.all([
      modelInstance1.fetch(data[0].key),
      modelInstance2.fetch(data[0].key)
    ])
    await modelInstance1.create(data[0].payload)
    const after = await Promise.all([
      modelInstance1.fetch(data[0].key),
      modelInstance2.fetch(data[0].key)
    ])
    expect(before[0]).to.deep.equal(before[1]).and.to.equal(undefined)
    expect(after[0]).to.not.equal(after[1])
  })

  it('should delete old instances', async () => {
    const manager = new ModelManager(RangeModel, 100)
    manager.getInstance('1')
    await new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 50)
    })
    manager.getInstance('2')
    expect(manager.history).to.have.length(2)
    await new Promise((resolve, reject) => {
      setTimeout(() => resolve(), 51)
    })
    manager.getInstance('3')
    expect(manager.history).to.have.length(2)
  })
})
