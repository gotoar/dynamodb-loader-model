const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

/** Parent class */
const { Model, ModelManager } = require('../classes/model')

/** Schema */
const config = {
  hashKey: 'id',
  rangeKey: '',
  fields: {
    id: { type: 'uuid' },
    name: { type: 'string', required: true },
    slug: { type: 'string' },
    domain: { type: 'string' },
    tournaments: { type: 'array' },
    pending: { type: 'object' },
    content: { type: 'object' }
  }
}

if (process.env.NODE_ENV === 'testing') {
  config.dynamo = {
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  }
}

/**
 * Team model class
 * @example
 * let model = new Model(tableName, { hashKey: 'id', fields: { ... } })
 */
class CustomerModel extends Model {
  /**
   * Pass the table and the config to the parent class
   */
  constructor () {
    /** Call parent constructor */
    super('table_customer', config)

    const methods = [
      '_formatTournament', '_formatDivision', '_slugExists',
      '_getNewSlug', 'fetchBySlug', 'createTournament',
      '_getTournament', 'updateTournament', 'removeTournament',
      'createDivision', '_getDivision', 'updateDivision',
      'removeDivision', 'addSeason', 'removeSeason'
    ]
    methods.forEach(method => {
      this[method] = this[method].bind(this)
    })
  }

  _formatTournament (data) {
    const tournamentSlug = data.name
    return {
      divisions: [],
      ...data,
      slug: tournamentSlug
    }
  }

  _checkRepeatedSlug (data) {
    let repeated = false
    const obj = {}
    data.forEach(el => {
      repeated = repeated || obj[el.slug]
      obj[el.slug] = true
    })
    return repeated
  }

  _formatDivision (division) {
    const divisionSlug = division.name
    return {
      seasons_id: [],
      ...division,
      slug: divisionSlug
    }
  }

  async _slugExists (slug) {
    const already = await this.fetchBySlug(slug)
    return already
  }

  async _getNewSlug (name, fix = true) {
    let tournamentSlug = name
    if (await this._slugExists(tournamentSlug)) {
      tournamentSlug += '-' + 'random'
    }
    return fix ? tournamentSlug : false
  }

  async fetchBySlug (slug) {
    return false
  }

  async createTournament (customerId, data) {
    const customer = await this.fetch(customerId)
    if (!customer) {
      throw new Error('Customer does not exist')
    }
    data = this._formatTournament(data)
    let newTournaments = customer.tournaments ? customer.tournaments.slice(0) : []
    newTournaments.push(data)
    if (this._checkRepeatedSlug(newTournaments)) {
      throw new Error('Slug already exists')
    }
    return this.update(customerId, { tournaments: newTournaments })
  }

  async _getTournament (customerId, tournamentIndex) {
    const customer = await this.fetch(customerId)
    if (!customer || !customer.tournaments[tournamentIndex]) {
      return {
        customer: false,
        tournament: false
      }
    }
    return {
      customer,
      tournament: customer.tournaments[tournamentIndex]
    }
  }

  async updateTournament (customerId, tournamentIndex, data) {
    let { customer, tournament } = await this._getTournament(customerId, tournamentIndex)
    if (!tournament) {
      throw new Error('Tournament does not exist')
    }
    data = this._formatTournament({
      ...tournament,
      ...data
    })
    customer.tournaments[tournamentIndex] = data
    if (this._checkRepeatedSlug(customer.tournaments)) {
      throw new Error('Slug already exists')
    }
    return this.update(customerId, customer)
  }

  async removeTournament (customerId, tournamentIndex) {
    const { customer, tournament } = await this._getTournament(customerId, tournamentIndex)
    if (!tournament) {
      throw new Error('Tournament does not exist')
    }
    if (tournament.divisions && tournament.divisions.length > 0) {
      throw new Error('You must remove all divisions first')
    }
    customer.tournaments[tournamentIndex] = null
    return this.update(customerId, customer)
  }

  async createDivision (customerId, tournamentIndex, data) {
    const { customer, tournament } = await this._getTournament(customerId, tournamentIndex)
    if (!tournament) {
      throw new Error('Tournament does not exist')
    }
    const formatted = this._formatDivision(data)
    if (!tournament.divisions) {
      tournament.divisions = []
    }
    await this.update(customerId, customer)
    tournament.divisions.push(formatted)
    if (this._checkRepeatedSlug(tournament.divisions)) {
      throw new Error('Slug already exists')
    }
    return this.update(customerId, customer)
  }

  async _getDivision (customerId, tournamentIndex, divisionIndex) {
    const customer = await this.fetch(customerId)
    if (!customer || !customer.tournaments[tournamentIndex] || !customer.tournaments[tournamentIndex].divisions[divisionIndex]) {
      return {
        customer: false,
        division: false
      }
    }
    return {
      customer,
      tournament: customer.tournaments[tournamentIndex],
      division: customer.tournaments[tournamentIndex].divisions[divisionIndex]
    }
  }

  async updateDivision (customerId, tournamentIndex, divisionIndex, data) {
    const { customer, division } = await this._getDivision(customerId, tournamentIndex, divisionIndex)
    if (!division) {
      throw new Error('Division does not exist')
    }
    const formatted = this._formatDivision({
      ...division,
      ...data
    })
    let newTournaments = customer.tournaments.slice(0)
    newTournaments[tournamentIndex].divisions[divisionIndex] = formatted
    if (this._checkRepeatedSlug(newTournaments)) {
      throw new Error('Slug already exists')
    }
    return this.update(customerId, { tournaments: newTournaments })
  }

  async removeDivision (customerId, tournamentIndex, divisionIndex) {
    const { customer, tournament, division } = await this._getDivision(customerId, tournamentIndex, divisionIndex)
    if (!division) {
      throw new Error('Division does not exist')
    }
    if (division.seasons_id && division.seasons_id.length > 0) {
      throw new Error('You must remove all seasons first')
    }
    tournament.divisions.splice(divisionIndex, 1)
    return this.update(customerId, customer)
  }

  async addSeason (customerId, tournamentIndex, divisionIndex, seasonId) {
    const { customer, division } = await this._getDivision(customerId, tournamentIndex, divisionIndex)
    if (!division) {
      throw new Error('Division does not exist')
    }
    if (!division.seasons_id) {
      division.seasons_id = []
    }
    await this.update(customerId, customer)
    division.seasons_id.push(seasonId)
    return this.update(customerId, customer)
  }

  async removeSeason (customerId, seasonId) {
    const customer = await this.fetch(customerId)
    let indexes = {
      tournament: undefined,
      division: undefined,
      season: undefined
    }
    customer.tournaments.forEach((tournament, ind1) => {
      if (tournament.divisions) {
        tournament.divisions.forEach((division, ind2) => {
          if (division.seasons_id) {
            const index = division.seasons_id.findIndex(comp => comp === seasonId)
            if (index !== -1) {
              indexes = {
                tournament: ind1,
                division: ind2,
                season: index
              }
            }
          }
        })
      }
    })
    if (indexes.season === undefined) {
      throw new Error('Season does not exist')
    }
    customer
      .tournaments[indexes.tournament]
      .divisions[indexes.division]
      .seasons_id.splice(indexes.season, 1)
    return this.update(customerId, customer)
  }
}

/** Export */
const manager = new ModelManager(CustomerModel, 30000)

const model = manager.getInstance('test')

const data = {
  valid: {
    id: '412kjcxz80',
    name: 'customer de prueba'
  },
  invalid: {
    id: 'i1287bcx'
  }
}

const tournamentData = {
  valid: {
    name: 'gran torneo'
  },
  valid2: {
    name: 'gran torneito 2'
  },
  invalid: {
    description: 'hola, soy un torneo'
  }
}

const divisionData = {
  valid: {
    name: 'division 1'
  },
  valid2: {
    name: 'division 2'
  },
  invalid: {
    description: 'hola, soy una division'
  }
}

describe('Creates a customer', () => {
  before(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
  })

  it('should create a customer', async () => {
    let res
    try {
      res = await model.create(data.valid)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('id').equal(data.valid.id)
  })

  it('should not create a customer without name', async () => {
    let res
    try {
      res = await model.create(data.invalid)
    } catch (err) {
      expect(err).to.have.property('message')
    }
    expect(res).to.equal(undefined)
  })
})

describe('Creates a tournament', () => {
  beforeEach(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
    await Promise.all([
      model.create(data.valid)
    ])
  })

  it('should create a tournament', async () => {
    let res
    try {
      res = await model.createTournament(data.valid.id, tournamentData.valid)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('name')
  })

  it('should create two tournaments', async () => {
    let res1
    let res2
    try {
      res1 = await model.createTournament(data.valid.id, tournamentData.valid)
      res2 = await model.createTournament(data.valid.id, tournamentData.valid2)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res2).to.have.property('tournaments').with.length(2)
    expect(res1.tournaments[0]).to.have.property('name')
    expect(res2.tournaments[0]).to.have.property('name')
  })

  it('should not create a tournament on a missing customer', async () => {
    let res
    try {
      res = await model.createTournament(data.invalid.id, tournamentData.valid)
    } catch (err) {
      expect(err).to.have.property('message')
    }
    expect(res).to.equal(undefined)
  })

  it('should not create a duplicate tournament', async () => {
    let res
    try {
      await model.createTournament(data.valid.id, tournamentData.valid)
      res = await model.createTournament(data.valid.id, tournamentData.valid)
    } catch (err) {
      expect(err).to.have.property('message')
    }
    expect(res).to.equal(undefined)
  })
})

describe('Updates a tournament', () => {
  beforeEach(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
    await Promise.all([
      model.create(data.valid)
    ])
  })

  it('should update a tournament', async () => {
    let res
    try {
      await model.createTournament(data.valid.id, tournamentData.valid)
      res = await model.updateTournament(data.valid.id, 0, { content: 'hey' })
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('name')
    expect(res.tournaments[0]).to.have.property('content')
  })
})

describe('Removes a tournament', () => {
  beforeEach(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
    await Promise.all([
      model.create(data.valid)
    ])
  })
  it('should remove the only tournament', async () => {
    let res, prev
    try {
      prev = await model.createTournament(data.valid.id, tournamentData.valid)
      expect(prev).to.have.property('tournaments').with.length(1)
      res = await model.removeTournament(data.valid.id, 0)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(0)
  })
  it('should remove the first tournament', async () => {
    let res, prev
    try {
      prev = await model.createTournament(data.valid.id, tournamentData.valid)
      prev = await model.createTournament(data.valid.id, tournamentData.valid2)
      expect(prev).to.have.property('tournaments').with.length(2)
      res = await model.removeTournament(data.valid.id, 0)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('name').equal(tournamentData.valid2.name)
  })
  it('should remove the second tournament', async () => {
    let res, prev
    try {
      prev = await model.createTournament(data.valid.id, tournamentData.valid)
      prev = await model.createTournament(data.valid.id, tournamentData.valid2)
      expect(prev).to.have.property('tournaments').with.length(2)
      res = await model.removeTournament(data.valid.id, 1)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('name').equal(tournamentData.valid.name)
  })
})

describe('Creates a division', () => {
  beforeEach(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
    await Promise.all([
      model.create(data.valid)
    ])
    await Promise.all([
      model.createTournament(data.valid.id, tournamentData.valid)
    ])
  })

  it('should create a division', async () => {
    let res
    try {
      res = await model.createDivision(data.valid.id, 0, divisionData.valid)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('divisions').with.length(1)
    expect(res.tournaments[0].divisions[0]).to.have.property('name').equal(divisionData.valid.name)
  })
})

describe('Adds a season', () => {
  beforeEach(async () => {
    await Promise.all([
      model.remove(data.valid.id)
    ])
    await Promise.all([
      model.create(data.valid)
    ])
    await Promise.all([
      model.createTournament(data.valid.id, tournamentData.valid)
    ])
    await Promise.all([
      model.createDivision(data.valid.id, 0, tournamentData.valid)
    ])
  })

  it('should create a season', async () => {
    const seasonId = 'k1jhxzc9'
    let res
    try {
      res = await model.addSeason(data.valid.id, 0, 0, seasonId)
    } catch (err) {
      expect(err).to.equal(undefined)
    }
    expect(res).to.have.property('tournaments').with.length(1)
    expect(res.tournaments[0]).to.have.property('divisions').with.length(1)
    expect(res.tournaments[0].divisions[0]).to.have.property('seasons_id').with.length(1)
    expect(res.tournaments[0].divisions[0].seasons_id[0]).to.equal(seasonId)
  })
})
