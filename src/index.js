/** Model class */
const Model = require('./classes/model')

/** Export */
module.exports = Model
