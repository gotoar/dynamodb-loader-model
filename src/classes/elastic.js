/**
 * Receives an endpoint and returns an elasticsearch client
 * @param {String|Array} endpoint AWS ElasticSearch endpoint
 * @returns elasticsearch client
 */
const createClient = (endpoint) => {
  const es = require('elasticsearch').Client({
    host: endpoint
  })
  return es
}

module.exports = createClient
