/** DataLoader */
const DataLoader = require('dataloader')
/** DynamoDB */
const Dynamo = require('./dynamo')
/** ElasticSearch */
const Elastic = require('./elastic')
/** UUID */
const uuidv4 = require('uuid/v4')
/** Simplified update queries */
const due = require('dynamo-update-expression')

/**
 * Global model class
 * @example
 * let model = new Model(
 *   tableName,
 *   {
 *     hashKey: 'id',
 *     fields: {
 *       id: {
 *         type: 'uuid'
 *       },
 *       name: {
 *         type: 'string',
 *         default: ''
 *       }
 *     }
 *   }
 * )
 */
class Model {
  /**
   * Create the model
   * @param {String} table Table name
   * @param {Object} config Config object
   * @param {String} config.hashKey Table hash key
   * @param {String} config.rangeKey Table range key
   * @param {String} config.fields Table schema, indicating type, default values and whether it's required or not
   * @param {Object} config.dynamo DynamoDB config
   * @param {String} config.dynamo.region DynamoDB region
   * @param {String} config.dynamo.endpoint DynamoDB endpoint
   */
  constructor (table, config) {
    /** Save config */
    this.config = config
    /** Table */
    this.table = table
    /** DynamoDB Client */
    this.Dynamo = new Dynamo(this.table, this.config.hashKey, this.config.rangeKey, config.dynamo)
    /** ElasticSearch client */
    if (config.elastic) {
      this.Elastic = Elastic(config.elastic)
    }
    /** DataLoader instance */
    const cacheKey = key => {
      return typeof key === 'object'
        ? Object.keys(key).sort().map(k => k + ':' + key[k]).join('-')
        : String(key)
    }
    this.loader = new DataLoader(keys => this.Dynamo.batchGet(keys), { cacheKeyFn: cacheKey })

    const methods = [
      'generateItem', 'create', 'remove', 'fetch',
      'batchFetch', 'update', '_generateModified',
      '_removeCommonProperties', 'search', '_removeEmptyStringElements'
    ]
    methods.forEach(method => {
      /** Binding */
      this[method] = this[method].bind(this)
    })
  }

  /**
   * Creates an item based on the schema passed in the config
   * @param {Object} data Item data
   * @param {Boolean} except Whether or not it throws an error when missing a required field. Defaults to false
   * @returns {Object} Populated item
   */
  generateItem (data, except = false) {
    const entries = Object.entries(this.config.fields)
    const obj = {}
    for (const [key, value] of entries) {
      if (value.required && !data[key]) {
        if (except) {
          throw new Error(`Missing a required field: ${key}`)
        }
        return false
      }
      if (value.type === 'uuid' && !data[key]) {
        data[key] = uuidv4()
      }
      const toinsert = data[key] !== undefined ? data[key] : value.default
      if (toinsert !== undefined && toinsert !== '') {
        obj[key] = toinsert
      }
    }
    return obj
  }

  /**
   * Removes empty string fields
   * @param {Object} obj Object to sanitize
   */
  _removeEmptyStringElements (obj) {
    for (var prop in obj) {
      if (typeof obj[prop] === 'object') { // dive deeper in
        this._removeEmptyStringElements(obj[prop])
      } else if (obj[prop] === '') { // delete elements that are empty strings
        delete obj[prop]
      }
    }
    return obj
  }

  /**
   * Creates an Item
   * @param {Object} raw Raw request data containing necessary fields
   * @returns {Promise<Object>} Created item
   */
  async create (raw) {
    const data = this._removeEmptyStringElements(this.generateItem(raw, true))
    let key = {}
    if (data[this.config.hashKey] && data[this.config.rangeKey]) {
      key[this.config.hashKey] = raw[this.config.hashKey]
      key[this.config.rangeKey] = raw[this.config.rangeKey]
    } else {
      key = raw[this.config.hashKey]
    }
    const res = await Promise.all([
      this.Dynamo.put(data),
      this.loader.clear(key)
    ])
    return res[0]
  }

  /**
   * Removes a user pool
   * @param {String|Object} key Key to fetch
   * @returns {Promise<Boolean>} True if successfully removed
   */
  async remove (key) {
    await Promise.all([
      this.Dynamo.remove(key),
      this.loader.clear(key)
    ])
    return true
  }

  /**
   * Fetches an item
   * @param {String|Object} key Key to fetch
   * @param {Boolean} fresh Whether to refresh the cache or not
   * @returns {Promise<Object>} Fetched item
   */
  async fetch (key, fresh = false) {
    if (fresh) {
      await this.loader.clear(key)
    }
    return this.loader.load(key)
  }

  /**
   * Fetches multiple items
   * @param {String|Object} key Key to fetch
   * @param {Boolean} fresh Whether to refresh the cache or not
   * @returns {Promise<Object>} Fetched item
   */
  batchFetch (keys, fresh = false) {
    return Promise.all(keys.map(key => this.fetch(key, fresh)))
  }

  /**
   * Creates an object to send to the update method
   * @param {Object} original Base object
   * @param {Object} passed New object
   * @param {Boolean} doNotMerge Whether to merge the new object with the base object or not
   */
  _generateModified (original, passed, doNotMerge = false) {
    return this._removeEmptyStringElements(!doNotMerge ? { ...original, ...passed } : passed)
  }

  /**
   * Removes the properties that are common to two objects
   * @param {Object} a Object to remove properties from
   * @param {Object} b Object to get properties from
   */
  _removeCommonProperties (a, b) {
    for (var property in b) {
      if (b.hasOwnProperty(property) && property !== this.config.hashKey && property !== this.config.rangeKey) {
        delete a[property]
      }
    }
    return a
  }

  /**
   * Updates an item
   * Clears cache for that item
   * @see
   * Sometimes we need to make a second run (that's why that do while is there) to
   * delete an empty object inside an array that remained from the first run when
   * it was "removed"
   * @param {String|Object} key Key to fetch
   * @param {Object} raw Raw request data
   * @param {Object|Boolean} options Update options
   * @property {Boolean} options.override Whether to remove missing fields or not
   * @property {Boolean} options.shallow Whether we want to avoid deep check of changes or not
   * @returns {Promise<Object>} Updated item
   */
  async update (key, raw, options = {}) {
    try {
      let original = await this.fetch(key, true)
      let modified = options === true || options.override
        ? this.generateItem(this._generateModified(original, raw, true), true)
        : this._generateModified(original, raw)
      if (options.shallow) {
        original = this._removeCommonProperties(original, modified)
      }
      let latest
      let done = false
      let loops = 0
      do {
        const updateExpression = due.getUpdateExpression({
          original: latest || original,
          modified
        })
        if (updateExpression.UpdateExpression && updateExpression.UpdateExpression !== '') {
          await this.Dynamo.update(key, updateExpression)
        }
        latest = await this.fetch(key, true)
        const diff = due.diff(latest, modified)
        done = diff.DELETE.length === 0
        loops += 1
      } while (!done && loops < 2)
      return latest
    } catch (err) {
      console.log(err)
      return false
    }
  }

  /**
   * Executes a query on ElasticSearch
   * @param {Object} query Query to execute
   * @returns {Object} ElasticSearch response
   */
  async search (query) {
    return this.Elastic.search(query)
  }
}

/**
 * ModelManager class
 */
class ModelManager {
  /**
   * Creates an instance of model manager
   * @param {Class} ManagedClass Class to manage
   * @param {Number} timeout Timeout to destroy instances, in milliseconds
   */
  constructor (ManagedClass, timeout) {
    /** Class to manage */
    this.ManagedClass = ManagedClass
    /** Model map */
    this.modelIndex = {}
    /** Instance ids with their created time */
    this.history = []
    /** Timeout to destroy instances */
    this.timeout = timeout

    const methods = [
      'getInstance', '_createInstance', '_clearTimeouted',
      'destroyInstance'
    ]
    methods.forEach(method => {
      /** Binding */
      this[method] = this[method].bind(this)
    })
  }

  /**
   * Returns an instance of the model, if it does not exist, it gets created
   * @param {String} id Instance id
   * @param {Boolean} create Whether to create an instance if it does not exist. Defaults to true
   * @returns {Class} Instance of the model
   */
  getInstance (id, create = true) {
    return !this.modelIndex[id] && create ? this._createInstance(id) : this.modelIndex[id]
  }

  /**
   * Creates an instance of the model and registers it in the history
   * @param {*} id Instance id
   * @returns {Class} Instance of the model
   */
  _createInstance (id) {
    this.modelIndex[id] = new this.ManagedClass()
    this.history.push({ id, t: Date.now() })
    if (this.timeout) {
      this._clearTimeouted()
    }
    return this.modelIndex[id]
  }

  /**
   * Checks the history of instances for timeouted models and destroys them
   */
  _clearTimeouted () {
    const now = Date.now()
    while (this.history.length && now > this.history[0].t + this.timeout) {
      this.destroyInstance(this.history[0].id)
      this.history.splice(0, 1)
    }
  }

  /**
   * Destroys an instance of the model
   * @param {String} id Instance id
   */
  destroyInstance (id) {
    if (this.modelIndex[id]) {
      delete this.modelIndex[id]
      this.modelIndex[id] = undefined
    }
  }
}

/** Export */
module.exports = {
  Model,
  ModelManager
}
