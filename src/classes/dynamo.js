/** AWS SDK */
let aws = require('aws-sdk')

/*
const config = new aws.Config({
  accessKeyId: process.env.DD_ACCESS_KEY_ID,
  secretAccessKey: process.env.DD_SECRET_ACCESS_KEY,
  region: process.env.DD_REGION
})
*/

/**
 * Returns a promise based DynamoDB Client
 * @param {String} table Table name
 * @param {String} hashKey Hash key
 * @param {String} rangeKey Range key
 * @returns {Object} DynamoDB Client
 */
class Dynamo {
  /**
   * Creates a promise based DynamoDB Client instance
   * @param {String} table Table to operate
   * @param {String} hashKey Hash key
   * @param {String} rangeKey Range key
   * @param {Object} config DynamoDB config
   * @param {String} config.region DynamoDB region
   * @param {String} config.endpoint DynamoDB endpoint
   */
  constructor (table, hashKey = '', rangeKey = '', config = {}) {
    /** Table to operate */
    this.table = table
    /** Hash key */
    this.hashKey = hashKey
    /** Range key */
    this.rangeKey = rangeKey
    /** DynamoDB Client */
    this.client = new aws.DynamoDB.DocumentClient(config)

    /** Binding */
    this.makeKey = this.makeKey.bind(this)
    /** Binding */
    this.get = this.get.bind(this)
    /** Binding */
    this.batchGet = this.batchGet.bind(this)
    /** Binding */
    this.put = this.put.bind(this)
    /** Binding */
    this.update = this.update.bind(this)
    /** Binding */
    this.query = this.query.bind(this)
    /** Binding */
    this.remove = this.remove.bind(this)
  }

  /**
   * Generates a dynamodb key.
   * If it receives a string, it uses it as hash key.
   * If it receives an object, it must have hash and range properties
   * @param {Object|String} el Key
   * @param {String} el.hash Hash key
   * @param {String} el.range Range key
   * @returns {Object} Formatted key
   */
  makeKey (el) {
    const obj = {}
    if (typeof el === 'string') {
      obj[this.hashKey] = el
    } else {
      obj[this.hashKey] = el[this.hashKey]
      obj[this.rangeKey] = el[this.rangeKey]
    }
    return obj
  }

  /**
   * Fetches a record
   * @param {Object|String} keys Keys to fetch by
   * @param {*} extra Extra DynamoDB DocumentClient parameters
   * @returns {Object} Record
   */
  get (keys, extra = {}) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: this.table,
        Key: this.makeKey(keys),
        ...extra
      }
      this.client.get(params, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data.Item)
      })
    })
  }

  /**
   * Fetches many records
   * @param {Object[]|String[]} keys Array of keys to fetch by
   * @param {*} extra Extra DynamoDB DocumentClient parameters
   * @returns {Object[]} Array of promises
   */
  batchGet (keys, extra = {}) {
    const promises = keys.map(el => {
      return this.get(el, extra)
    })
    return Promise.all(promises)
  }

  /**
   * Inserts a record
   * @param {Object} item Record to insert
   * @param {*} extra Extra DynamoDB DocumentClient parameters
   * @returns {Object} Inserted record
   */
  put (item, extra = {}) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: this.table,
        Item: item,
        ...extra
      }
      this.client.put(params, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(item)
      })
    })
  }

  /**
   * Updates a record
   * @param {Object|String} keys Record keys to update
   * @param {*} expression Update expression
   * @param {*} extra Extra DynamoDB DocumentClient parameters
   * @returns {Object} Updated record
   */
  update (keys, expression, extra = {}) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: this.table,
        Key: this.makeKey(keys),
        ReturnValues: 'UPDATED_NEW',
        ...expression,
        ...extra
      }
      this.client.update(params, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data)
      })
    })
  }

  /**
   * Custom query
   * @param {*} expression expression
   * @param {*} values values
   * @param {*} filter filter
   * @param {*} extra extra
   * @returns {Object[]} Records
   */
  query (expression, values, filter = undefined, extra = {}) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: this.table,
        KeyConditionExpression: expression,
        FilterExpression: filter,
        ExpressionAttributeValues: values,
        ...extra
      }
      this.client.query(params, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data)
      })
    })
  }

  /**
   * Removes a record
   * @param {Object|String} keys Record keys to remove
   * @param {*} extra Extra DynamoDB DocumentClient parameters
   * @returns {Boolean} True if removed successfully
   */
  remove (keys, extra = {}) {
    return new Promise((resolve, reject) => {
      const params = {
        TableName: this.table,
        Key: this.makeKey(keys),
        ...extra
      }
      this.client.delete(params, (err, data) => {
        if (err) {
          reject(err)
          return
        }
        resolve(data)
      })
    })
  }
}

/** Export */
module.exports = Dynamo
