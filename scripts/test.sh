#!/bin/bash
echo "Installing DynamoDB..."
sls dynamodb install
echo "Starting DynamoDB..."
sls dynamodb start --inMemory --migrate
echo "DynamoDB installed and started, running tests."
if (( "$#" == 1 )); then
  npm run testonlywatch &
  read -n1 -r -p "(Press any key to exit)" key
  kill $(ps aux | grep '[m]ocha' | awk '{print $2}')
else
  npm run testonly
fi
kill $(ps aux | grep '[D]ynamo' | awk '{print $2}')